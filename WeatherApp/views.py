from django.shortcuts import render, get_object_or_404
import requests
from .models import City, Weather_Data
from .forms import CityForm


# Create your views here.

def index(request):
    url = "https://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=7acb08d0ef5c71e4cc6cb03898056220"

    if request.method == 'POST':
        form = CityForm(request.POST)

        if form.is_valid():
            new = form.cleaned_data['name']
            count = City.objects.filter(name=new).count()
            if count == 0:
                re = requests.get(url.format(new)).json()
                if re['cod'] == 200:
                    form.save()

    form = CityForm()

    cities = City.objects.all()

    weather_list = []

    for city in cities:

        r = requests.get(url.format(city.name)).json()

        try:
            deg = r["wind"]["deg"]
        except:
            deg = 0

        data_dictionary = {
            "coord_lon": r["coord"]["lon"],
            "coord_lat": r["coord"]["lat"],

            "weather_main": r["weather"][0]["main"],
            "weather_description": r["weather"][0]["description"],
            "weather_icon": r["weather"][0]["icon"],

            "main_temp": r["main"]["temp"],
            "main_feels_like": r["main"]["feels_like"],
            "main_temp_min": r["main"]["temp_min"],
            "main_temp_max": r["main"]["temp_max"],
            "main_pressure": r["main"]["pressure"],
            "main_humidity": r["main"]["humidity"],

            "wind_speed": r["wind"]["speed"],
            "wind_deg": deg
        }

        savedata = Weather_Data(coord_lon=data_dictionary["coord_lon"], coord_lat=data_dictionary["coord_lat"],
                                weather_main=data_dictionary["weather_main"],
                                weather_description=data_dictionary["weather_description"],
                                weather_icon=data_dictionary["weather_icon"],
                                main_temp=data_dictionary["main_temp"],
                                main_feels_like=data_dictionary["main_feels_like"],
                                main_temp_min=data_dictionary["main_temp_min"],
                                main_temp_max=data_dictionary["main_temp_max"],
                                main_pressure=data_dictionary["main_pressure"],
                                main_humidity=data_dictionary["main_humidity"],
                                wind_speed=data_dictionary["wind_speed"],
                                wind_deg=data_dictionary["wind_deg"],
                                city=city)
        savedata.save()

        weather_dictionary = {
            "city": city.name,
            "temperature": r["main"]["temp"],
            "description": r["weather"][0]["description"],
            "icon": r["weather"][0]["icon"]
        }

        weather_list.append(weather_dictionary)

    context = {
        'weather_list': weather_list,
        'form': form
    }

    return render(request, 'index.html', context)

def weather_detail(request, id):
    obj = get_object_or_404(Weather_Data, id=id)
    context = {
        "object": obj
    }
    return render(request, 'weather_detail.html', context)

def weather_list(request, name):
    bfr = City.objects.filter(name=name)
    obj = bfr[0]
    queryset = Weather_Data.objects.filter(city=obj)
    context = {
        "object_list": queryset
    }
    return render(request, "weather_list.html", context)

def weather_graph(request, name):
    bfr = City.objects.filter(name=name)
    obj = bfr[0]
    queryset = Weather_Data.objects.filter(city=obj)
    context = {
        "name" : name,
        "object_list": queryset
    }
    return render(request, "weather_graph.html", context)
