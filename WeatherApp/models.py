from django.db import models

# Create your models here.

class City(models.Model):
    name = models.CharField(max_length=40)

    class Meta:
        verbose_name_plural = 'cities'


class Weather_Data(models.Model):
    coord_lon = models.FloatField()
    coord_lat = models.FloatField()

    weather_main = models.CharField(max_length=40)
    weather_description = models.CharField(max_length=100)
    weather_icon = models.CharField(max_length=10)

    main_temp = models.FloatField()
    main_feels_like = models.FloatField()
    main_temp_min = models.FloatField()
    main_temp_max = models.FloatField()
    main_pressure = models.IntegerField()
    main_humidity = models.IntegerField()

    wind_speed = models.FloatField()
    wind_deg = models.IntegerField()

    city = models.ForeignKey(City, on_delete=models.CASCADE)

    date_time = models.DateTimeField(null=True, blank=True, default=None)
